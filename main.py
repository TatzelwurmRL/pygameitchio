import pygame
import pygame.freetype
import sys
import os 
import re
from collections import namedtuple
from random import choice, randint
from itertools import cycle

natsort = lambda s: [int(t) if t.isdigit() else t.lower() for t in re.split('(\d+)', s)]

RESOLUTION = 800, 600
FPS = 60
TILESIZE = 32
PLAYER_SPEED = 300
FALLING_SPEED = 400
ANIM_THRESHOLD = 0.05

Achievement = namedtuple('Achievement', 'key message condition')

ACHIEVEMENTS = {a.key: a for a in [
    Achievement('5_POINTS', 'Earn 5 points in the game', lambda p: p.score >= 5),
    Achievement('DO_NOTHING', 'Do not move for 5 seconds', lambda p: p.standing_still_timer >= 5)
]}

locked = {a for a in ACHIEVEMENTS}

def load_images(dir):
    bundle_dir = getattr(sys, '_MEIPASS', os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data'))
    path = os.path.join(bundle_dir, dir)
    for f in sorted(os.listdir(path), key=natsort):
        yield pygame.image.load(os.path.join(path, f)).convert_alpha()

class Player(pygame.sprite.Sprite):
    def __init__(self, pos, falling_stuff, *grps):
        super().__init__(*grps)
        idle_images = list(load_images('1-Idle'))
        run_images = list(load_images('2-Run'))
        self.images = {
            'IDLE': {
                'RIGHT': cycle(idle_images),
                'LEFT': cycle(pygame.transform.flip(s, True, False) for s in idle_images)
            },
            'RUN': {
                'RIGHT': cycle(run_images),
                'LEFT': cycle(pygame.transform.flip(s, True, False) for s in run_images)
            }
        }
        self.state = 'IDLE'
        self.direction = 'RIGHT'
        self.image = next(self.images['IDLE']['RIGHT'])
        self.animation_counter = 0
        self.rect = self.image.get_frect(topleft=pos)
        self.falling_stuff = falling_stuff
        self.score = 0
        self.hitbox = pygame.Rect(0, 0, 30, 20)
        self.hitbox.center = self.rect.center
        self.hitbox.move_ip(-10, -10)
        self.standing_still_timer = 0

    def update_image(self, dt, new_state):
        self.animation_counter += dt
        if self.animation_counter > ANIM_THRESHOLD or self.state != new_state:
            self.image = next(self.images[new_state][self.direction])
            self.animation_counter = 0

    def update(self, dt, events):
        d = 0
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_a]: d -= 1
        if pressed[pygame.K_d]: d += 1

        self.rect.move_ip(d * dt * PLAYER_SPEED, 0)
        display_rect = pygame.display.get_surface().get_rect()
        self.rect.clamp_ip(display_rect)

        if d == 1:
            new_state = 'RUN'
            self.direction = 'RIGHT'
        if d == -1:
            new_state = 'RUN'
            self.direction = 'LEFT'
        if d == 0:
            new_state = 'IDLE'

        for stuff in self.falling_stuff:
            if self.hitbox.colliderect(stuff.rect):
                stuff.kill()
                self.score += 1

        if self.state == 'IDLE':
            self.standing_still_timer += dt
        if new_state != 'IDLE':
            self.standing_still_timer = 0

        self.update_image(dt, new_state)
        self.state = new_state
        
        self.hitbox.center = self.rect.center
        self.hitbox.move_ip(10 if self.direction == 'LEFT' else -10, -10)
        pygame.draw.rect(pygame.display.get_surface(), 'red', self.hitbox, 2)

class FallingStuff(pygame.sprite.Sprite):
    def __init__(self, pos, *grps):
        super().__init__(*grps)
        self.image = pygame.Surface((TILESIZE, TILESIZE))
        self.image.fill(choice(['red', 'yellow', 'green']))
        self.rect = self.image.get_rect(topleft=pos)
        
    def update(self, dt, events):
        self.rect.move_ip(0, FALLING_SPEED * dt)
        display_rect = pygame.display.get_surface().get_rect()
        if self.rect.top > display_rect.bottom:
            self.kill()
            
class AchievementMessage(pygame.sprite.Sprite):
    font = None
    def __init__(self, message, *grps):
        super().__init__(*grps)
        if not AchievementMessage.font:
            AchievementMessage.font = pygame.freetype.SysFont('Arial', 32)
        f = AchievementMessage.font
        
        self.image = pygame.Surface((400, 80))
        self.image.fill('darkgrey')
        self.rect = self.image.get_rect(topright=pygame.display.get_surface().get_rect().topright)

        f.render_to(self.image, (49, 11), 'Achievement unlocked!', 'black', size=24)
        f.render_to(self.image, (50, 10), 'Achievement unlocked!', 'white', size=24)
        
        f.render_to(self.image, ( 9, 51), message, 'black', size=32)
        f.render_to(self.image, (10, 50), message, 'white', size=32)
        
        self.timeout = 5
        
    def update(self, dt, events):
        self.timeout -= dt
        if self.timeout <= 0:
            self.kill()

def main():
    pygame.init()
    screen = pygame.display.set_mode(RESOLUTION)
    dt, clock = 0, pygame.time.Clock()
    sprites = pygame.sprite.Group()
    falling_stuff = pygame.sprite.Group()
    player = Player((300, 500), falling_stuff, sprites)
    font = pygame.freetype.SysFont('Arial', 54)

    CREATE_STUFF = pygame.USEREVENT + 1
    pygame.time.set_timer(CREATE_STUFF, randint(1000, 2000), True)
    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT:
                return
            if e.type == CREATE_STUFF:
                pygame.time.set_timer(CREATE_STUFF, randint(1000, 2000), True)
                FallingStuff((randint(50, 550), -TILESIZE), falling_stuff, sprites)
        screen.fill('black')
        font.render_to(screen, (20, 20), f'Score: {player.score}', 'white')
        sprites.draw(screen)
        sprites.update(dt, events)
        
        unlocked = {key for key in locked if ACHIEVEMENTS[key].condition(player)}
        for key in unlocked:
            AchievementMessage(ACHIEVEMENTS[key].message, sprites)
            locked.remove(key)
        
        pygame.display.flip()
        dt = clock.tick(FPS) / 1000

if __name__ == "__main__":
    main()